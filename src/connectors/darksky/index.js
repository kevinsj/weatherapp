import axios from 'axios';
import helper from './helper.js';
import { UpStreamError } from '../../middleware/Error.js';

/**
 *
 *
 * @param {Number} latitude
 * @param {Number} longitude
 * @param {boolean} [transform=true]
 * @param {string} [unit='si']
 */
const getWeather = async (
  latitude,
  longitude,
  transform = true,
  unit = 'si'
) => {
  const darkskyQueryAddress = helper.generateDarkSkyQuery(
    latitude,
    longitude,
    unit
  );
  try {
    const { data: rawData } = await axios.get(darkskyQueryAddress);
    const validatedResult = await helper.DARK_SKY_RESPONSE_SCHEMA.validateAsync(
      rawData,
      { allowUnknown: true }
    );
    return transform ? helper.transform(validatedResult) : rawData;
  } catch (err) {
    if (err.isJoi) {
      console.error('Darksky returned invalid response', err);
      throw new UpStreamError('Darksky', 'Darksky returned invalid response');
    } else if (axios.isAxiosError(err)) {
      const { response } = err;
      const { request, ...errorObject } = response; // take everything but 'request'
      console.error('Error fetching from Darksky ', errorObject);
    }
    throw err;
  }
};

export default { getWeather };
