import Joi from 'joi';

const DARK_SKY_ADDRESS =
  'https://api.darksky.net/forecast/' + process.env.DARK_SKY_KEY;

const generateDarkSkyQuery = (
  /** @type {Number} */ latitude,
  /** @type {Number} */ longitude,
  /** @type {string} */ unit = 'si'
) => `${DARK_SKY_ADDRESS}/${latitude},${longitude}` + `?unit=${unit}`;

const DARK_SKY_RESPONSE_SCHEMA = Joi.object({
  latitude: Joi.number().required(),
  longitude: Joi.number().required(),
  currently: Joi.object({
    summary: Joi.string().required(),
    time: Joi.number().required(),
    temperature: Joi.number().required(),
    windSpeed: Joi.number().required(),
    windBearing: Joi.number().required(),
  }),
  hourly: Joi.object({
    summary: Joi.string().required(),
    icon: Joi.string().required(),
    data: Joi.array()
      .items(
        Joi.object({
          summary: Joi.string().required(),
          time: Joi.number().required(),
          temperature: Joi.number().required(),
          windSpeed: Joi.number().required(),
          windBearing: Joi.number().required(),
        })
      )
      .required(),
  }),
});

const transformHourlyData = (
  /** @type {{ time: Number; temperature: Number; }[]} */ hourlyData
) =>
  hourlyData.map(({ time, temperature }) => ({
    timestamp: new Date(time * 1000).toLocaleString(),
    temperature,
  }));

const transform = (
  /** @type {{ currently: any; hourly: any; }} */ rawResult
) => {
  const { currently, hourly = { data: [] } } = rawResult;
  const { summary: currentSummary, windSpeed, windBearing } = currently;
  const day = transformHourlyData(hourly.data);

  const wind = {
    speed: windSpeed,
    direction: windBearing,
  };

  return {
    summary: currentSummary,
    wind,
    day,
  };
};

export default {
  DARK_SKY_ADDRESS,
  DARK_SKY_RESPONSE_SCHEMA,
  generateDarkSkyQuery,
  transform,
};
