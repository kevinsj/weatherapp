export class IdNotFoundError extends Error {
  /**
   * @param {string} id
   * @param {string} err
   */
  constructor(id, err) {
    super(err);
    this.id = id;
  }
}

export class UpStreamError extends Error {
  /**
   * @param {string} connector
   * @param {string} err
   */
  constructor(connector, err) {
    super(err);
    this.connector = connector;
  }
}
