import createHttpError from 'http-errors';
import Joi from 'joi';

/**
 *
 *
 * @param {string} type
 * @param {Joi.Schema} schema
 * @returns
 */
const validateRequest = (type, schema) => {
  return async (
    /** @type {{ [x: string]: any; body: any; }} */ req,
    /** @type {any} */ _res,
    /** @type {(arg0: createHttpError.HttpError) => void} */ next
  ) => {
    try {
      const validated = await schema.validateAsync(req[type]);
      req.body = validated;
      next();
    } catch (err) {
      if (err.isJoi)
        return next(createHttpError(400, { message: err.message }));
    }
  };
};

const validateBody = (/** @type {Joi.Schema<any>} */ schema) =>
  validateRequest('body', schema);

const validateQuery = (/** @type {Joi.Schema<any>} */ schema) =>
  validateRequest('query', schema);

export { validateBody, validateQuery };
