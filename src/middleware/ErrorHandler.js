import createHttpError from 'http-errors';
import { IdNotFoundError, UpStreamError } from './Error.js';

export default (
  /** @type {{ message: any; }} */ error,
  /** @type {any} */ _req,
  /** @type {any} */ _res,
  /** @type {any} */ next
) => {
  switch (error.constructor) {
    case IdNotFoundError:
      return next(createHttpError(404, { message: error.message }));
    case UpStreamError:
      return next(createHttpError(500, { message: error.message }));
    default:
      next(error);
  }
};
