export default (
  /** @type {any} */ _req,
  /** @type {any} */ _res,
  /** @type {any} */ next
) => {
  req.logger.info('');
  next();
};
