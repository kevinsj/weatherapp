import express from 'express';

import location from './location.js';
import forecast from './forecast.js';

const router = express.Router();

router.use('/location', location);
router.use('/forecast', forecast);

export default router;
