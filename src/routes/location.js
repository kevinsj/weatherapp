import Joi from 'joi';
import express from 'express';
import { validateBody } from '../middleware/Validator.js';
import locationHandler from '../handlers/location.js';

const locationReqSchema = Joi.object({
  id: Joi.string().uuid().required(),
  longitude: Joi.number().required(),
  latitude: Joi.number().required(),
});

const router = express.Router();

router.post('/', validateBody(locationReqSchema), (req, res) => {
  const { id, latitude, longitude } = req.body;

  locationHandler(id, latitude, longitude);

  res.send(req.body);
});

export default router;
