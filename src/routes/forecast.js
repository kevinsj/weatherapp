import Joi from 'joi';
import express from 'express';
import { validateQuery } from '../middleware/Validator.js';
import { idToLocation } from '../handlers/location.js';
import forcastHandler from '../handlers/forecast.js';

const forcastReqSchema = Joi.object({
  id: Joi.string().uuid().required(),
});

const router = express.Router();

router.get('/', validateQuery(forcastReqSchema), async (req, res, next) => {
  try {
    const { id } = req.query;
    const forecast = await forcastHandler(id, idToLocation);
    res.send(forecast);
  } catch (err) {
    next(err);
  }
});

export default router;
