import { Darksky } from '../connectors/index.js';
import { IdNotFoundError } from '../middleware/Error.js';

export const weatherLookupCache = new Map();

export default async (
  /** @type {any} */ id,
  /** @type {{ [x: string]: any; }} */ idToLocation
) => {
  const coordinates = idToLocation[id];
  if (!coordinates) throw new IdNotFoundError(id, ' id not found');
  const { latitude, longitude } = coordinates;
  const cacheKey = `${latitude.toFixed(3)}-${longitude.toFixed(3)}`;
  console.log(cacheKey);
  const isCached = weatherLookupCache.has(cacheKey);
  if (isCached) return weatherLookupCache.get(cacheKey);
  const weather = await Darksky.getWeather(latitude, longitude, true);
  weatherLookupCache.set(cacheKey, weather);
  return weather;
};
