export const idToLocation = {};
export default (
  /** @type {string} */ id,
  /** @type {number} */ latitude,
  /** @type {number} */ longitude
) => {
  idToLocation[id] = { latitude, longitude };
};
