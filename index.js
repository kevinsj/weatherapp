import './env.js';
import express from 'express';
import pino from 'pino-http';
import cors from 'cors';
import routes from './src/routes/index.js';
import ErrorHandler from './src/middleware/ErrorHandler.js';

const PORT = process.env.PORT || 3000;
const app = express();
const logger = pino();

app.use(logger);
app.use(cors());
app.use(express.json());
app.use('/api', routes);
app.use(ErrorHandler);

// function handle(req, res) {
// logger(req, res);
// }

const server = app.listen(PORT, () => {
  console.debug('Listen on port ' + server.address().port);
});

export default server;
