import { expect } from 'chai';
import sinon from 'sinon';
import Darksky from '../../src/connectors/darksky/index.js';
import forecast, { weatherLookupCache } from '../../src/handlers/forecast.js';

describe('Forcast middleware', function () {
  afterEach(function () {
    sinon.restore();
  });
  it('- Should throw id not found error if id not found', async function () {
    const id = 'c567f87e-d751-43e3-93ed-554470cca7ae';
    try {
      await forecast(id, {});
    } catch (e) {
      expect(e.id).to.equal(id);
      expect(e.message).to.equal(' id not found');
    }
  });
  it('- Should do weather lookup and update cache for valid id if not cached', async function () {
    const id = 'c567f87e-d751-43e3-93ed-554470cca7ae';
    const coordinates = { latitude: 10.1234567, longitude: 10.1234567 };
    const cacheKey = `${coordinates.latitude.toFixed(
      3
    )}-${coordinates.longitude.toFixed(3)}`;
    const idToLocation = {
      [id]: coordinates,
    };
    const mockedWeather = { summary: '' };
    const getWeatherStub = sinon
      .stub(Darksky, 'getWeather')
      .callsFake(sinon.fake.resolves(mockedWeather));
    expect(weatherLookupCache).to.not.have.key(cacheKey);
    const weatherResult = await forecast(id, idToLocation);
    getWeatherStub.calledOnceWith(
      coordinates.latitude,
      coordinates.longitude,
      true
    );
    expect(weatherLookupCache).to.have.key(cacheKey);
    expect(weatherLookupCache.get(cacheKey)).to.deep.equal(mockedWeather);
    expect(weatherResult).to.deep.equal(mockedWeather);
  });
  it('- Should return from cache without downstream lookup if coordinates in cache', async function () {
    const id = 'c567f87e-d751-43e3-93ed-554470cca7ae';
    const coordinates = { latitude: 10.1234567, longitude: 10.1234567 };
    const cacheKey = `${coordinates.latitude.toFixed(
      3
    )}-${coordinates.longitude.toFixed(3)}`;
    const idToLocation = {
      [id]: coordinates,
    };
    const mockedWeather = { summary: '' };
    const getWeatherStub = sinon
      .stub(Darksky, 'getWeather')
      .callsFake(sinon.fake.resolves(mockedWeather));
    expect(weatherLookupCache).to.have.key(cacheKey);
    const weatherResult = await forecast(id, idToLocation);
    sinon.assert.notCalled(getWeatherStub);
    expect(weatherLookupCache).to.have.key(cacheKey);
    expect(weatherResult).to.deep.equal(mockedWeather);
  });
});
