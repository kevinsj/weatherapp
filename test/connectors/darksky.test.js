import axios from 'axios';
import sinon from 'sinon';
import { expect } from 'chai';
import { afterEach } from 'mocha';

import { darkskyMock, darkskyTransformed } from './darksky_mock.js';
import helper from '../../src/connectors/darksky/helper.js';
import Darksky from '../../src/connectors/darksky/index.js';
import { UpStreamError } from '../../src/middleware/Error.js';

describe('Darksky connector', function () {
  describe('- Darksky connector', function () {
    const mockArgs = [0, 0, false];
    const mockAddress = 'https://example.com';
    let queryHelperStub;
    let transformHelperSpy;

    beforeEach(function (done) {
      queryHelperStub = sinon
        .stub(helper, 'generateDarkSkyQuery')
        .callsFake(sinon.fake.resolves(mockAddress));
      transformHelperSpy = sinon.spy(helper, 'transform');

      done();
    });
    afterEach(function (done) {
      sinon.restore();
      done();
    });

    it('- Should rethrow error when axios throws error', async function () {
      const axiosStub = sinon
        .stub(axios, 'get')
        .callsFake(sinon.fake.throws(new Error('axios error')));
      try {
        await Darksky.getWeather(...mockArgs);
      } catch (e) {
        expect(e.message).to.equal('axios error');
      }
      queryHelperStub.calledOnceWith(...mockArgs);
      axiosStub.calledOnceWith(mockAddress);
    });

    it('- Should throw upstream error when returned data failed joi validation', async function () {
      const axiosStub = sinon.stub(axios, 'get').callsFake(
        sinon.fake.resolves({
          data: {},
        })
      );
      try {
        await Darksky.getWeather(...mockArgs);
      } catch (e) {
        expect(e instanceof UpStreamError).to.equal(true);
        expect(e.connector).to.equal('Darksky');
        expect(e.message).to.equal('Darksky returned invalid response');
      }
      queryHelperStub.calledOnceWith(...mockArgs);
      axiosStub.calledOnceWith(mockAddress);
    });

    it('- Should return raw result if transform is false ', async function () {
      const axiosStub = sinon.stub(axios, 'get').callsFake(
        sinon.fake.resolves({
          data: darkskyMock,
        })
      );
      const weather = await Darksky.getWeather(0, 0, false);
      queryHelperStub.calledOnceWith(0, 0, false);
      axiosStub.calledOnceWith(mockAddress);
      transformHelperSpy.calledOnceWith(darkskyMock);
      expect(weather).to.deep.equal(darkskyMock);
    });

    it('- Should return transformed result if transform is false ', async function () {
      const axiosStub = sinon.stub(axios, 'get').callsFake(
        sinon.fake.resolves({
          data: darkskyMock,
        })
      );
      const weather = await Darksky.getWeather(0, 0);
      queryHelperStub.calledOnceWith(0, 0);
      axiosStub.calledOnceWith(mockAddress);
      transformHelperSpy.calledOnceWith(darkskyMock);
      expect(weather).to.deep.equal(darkskyTransformed);
    });
  });

  describe('- Darksky connector helper', function () {
    describe('- Transform data helper ', function () {
      it('- Should transform data', function () {
        const transformedData = helper.transform(darkskyMock);

        expect(transformedData).to.have.keys(['summary', 'wind', 'day']);
        expect(transformedData.day).to.have.lengthOf(
          darkskyMock.hourly.data.length
        );
        expect(transformedData).to.deep.equal(darkskyTransformed);
      });
    });
  });
});
