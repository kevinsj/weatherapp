import supertest from 'supertest';
import { expect } from 'chai';
import sinon from 'sinon';
import server from '../../index.js';
import Darksky from '../../src/connectors/darksky/index.js';

describe('Forcast route integration test', function () {
  afterEach(function () {
    sinon.restore();
  });
  describe('Failure test', function () {
    it('- Should return 404 for non-existent id', function (done) {
      supertest(server)
        .get('/api/forecast?id=cf3c8351-d4e1-4865-a27d-7b195155ca01')
        .expect(404, done);
    });
    it('- Should return bad request for invalid id format', function (done) {
      supertest(server).get('/api/forecast?id=9999999').expect(400, done);
    });
  });
  describe('Happy path', function () {
    it('- Should return 200 for valid id', async function () {
      const mockedWeather = { summary: '' };
      sinon
        .stub(Darksky, 'getWeather')
        .callsFake(sinon.fake.resolves(mockedWeather));
      const app = supertest(server);
      const res = await app
        .post('/api/location')
        .send({
          id: 'b5df8423-9f17-480a-9b06-98a7184a10ba',
          latitude: 10.11111,
          longitude: 10.11111,
        })
        .expect(200);
      const id = res.body.id;
      return app
        .get(`/api/forecast?id=${id}`)
        .expect(200)
        .then((res) => {
          expect(res.body).to.deep.equal(mockedWeather);
        });
    });
  });
});
