### Environment variable
To get started, fill in the variables in the .env file.

### Endpoints implemented
- POST /api/location
    - Takes a pair of coordinates along with an id and store in memory

- GET /api/weather
    - Takes an id as query parameter, and retrieves data from darksky. Results returned from darksky will be cached based on coordinates. If two ids shares the same coordinates (up to 3 decimals) the same, then the cached result will be returned

### Docker
A simple multi-staged docker image was implemented. To build run `docker build -t weatherapp:latest .` and `docker run -p 3000:3000 weatherapp:latest` the app will be available at port 3000
